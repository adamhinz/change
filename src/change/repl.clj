(ns change.repl
  "Helpful functions for matching pdf io"
  (:require [change.core :refer :all]))

(defn show!
  ([reg] (show! reg true))
  ([reg dollars?]
   (println (show reg dollars?))
   reg))

(defn change! [reg chg]
  (let [{:keys [register change error]} (change reg chg)]
    (if error
      (println "sorry")
      (show! change false))
    register))

(defn repl-example []
  (-> empty-register
      (add-bills {20 1, 10 2, 5 3, 2 4, 1 5})
      show!

      (add-bills {20 1, 10 2, 5 3, 2 0, 1 5})
      show!

      (take-bills {20 1, 10 4, 5 3, 2 0, 1 10})
      show!

      (change! 11)
      show!

      (change! 14)))

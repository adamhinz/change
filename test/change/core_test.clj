(ns change.core-test
  (:require [clojure.test :refer :all]
            [change.core :refer :all]
            [clojure.test.check.clojure-test :refer [defspec]]
            [clojure.test.check :as tc]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]))

(def n-tests
  "Number of generative tests to run"
  1000)

(deftest simple-changes
  (testing "ignores invalid bills"
    (is (= (-> empty-register
               (add-bills {1000 1}))
           empty-register)))

  (testing "doesn't use a naive approach"
    (let [chg (-> empty-register
                  (add-bills {5 1, 2 3})
                  (change 6)
                  :change)]
      (is (= chg {20 0, 10 0, 5 0, 2 3, 1 0}))))

  (testing "not enough change"
    (let [base-register
          (-> empty-register
              (add-bills {20 1, 10 0, 5 2, 2 1, 1 0}))

          {:keys [change register error]
           :as thing}
          (change base-register 14)]
      (is (= register base-register))
      (is (= change nil))
      (is (= error :no-change))))

  (testing "simple change"
    (let [{:keys [change register error]}
          (-> empty-register
              (add-bills {20 1, 10 0, 5 3, 2 4, 1 0})
              (change 11))]
      (is (= nil error))
      (is (= register
             {20 1, 10 0, 5 2, 2 1, 1 0}))
      (is (= change
             {20 0, 10 0, 5 1, 2 3, 1 0}))))

  (testing "can show the register"
    (let [reg1 (-> empty-register
                   (add-bills {20 1, 10 2, 5 3, 2 4, 1 5}))
          show-reg1 (show reg1)

          reg2 (-> reg1
                   (add-bills {20 1, 10 2, 5 3, 2 0, 1 5}))

          show-reg2 (show reg2)

          reg3 (-> reg2
                   (take-bills {20 1, 10 4, 5 3, 2 0, 1 10}))

          show-reg3 (show reg3)]
      (is (= show-reg1 "$68 1 2 3 4 5"))
      (is (= show-reg2 "$128 2 4 6 4 10"))
      (is (= show-reg3 "$43 1 0 3 4 0"))))

  (testing "Can add to register"
    (let [reg
          (-> empty-register
              (add-bills {10 5 2 1})
              (add-bills {10 2 1 5})
              (add-bills {20 2 5 1}))]
      (is (= reg {20 2
                  10 7
                  5 1
                  2 1
                  1 5})))))

(defn build-register
  "Given a seq of denominations with length n and a seq of
   random numbers with length n*2 return a map like:

   {:register <a cash register with some bills>
    :change-total <some amount of change that can be returned>}
  "
  [denoms rands]
  (let [amts (->> rands
                  (partition 2 2)
                  (map (fn [denom [a b]]
                         [[denom (max a b)] (* denom (min a b))])
                       denoms))

        total (reduce + 0 (map last amts))
        reg (into {} (map first amts))]
    {:register reg
     :change-total total}))

(defn create-register-generator [denoms]
  (gen/fmap (partial build-register denoms)
            (gen/vector gen/nat (* 2 (count denoms)))))

(def register-generator
  "A test.check generator for creating registers"
  (create-register-generator denoms))

(defspec can-generate-change
  n-tests
  (prop/for-all [{:keys [register change-total]}
                 register-generator]
                (let [val (->> change-total
                               (change register)
                               :change
                               dollar-value)]
                  (= val change-total))))

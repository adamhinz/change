(ns change.core
  "Functions for generating change for a given register

   A bill set is a map with keys from `denoms` where
   the values represent the # of bills for that `denom`"
  (:require [clojure.string :as string]))

(def denoms
  "Denominations that our system can handle"
  [20 10 5 2 1])

(def empty-register
  "The empty register that contains no cash"
  (into {} (map (fn [denom] [denom 0]) denoms)))

(defn combine-bills
  "Combine a register with a bill set using the given
   combinator"
  [combine reg amts]
  (->> denoms
       (map (fn [denom]
              [denom (combine (get reg denom 0)
                              (get amts denom 0))]))
       (into {})))

(def add-bills
  "Add a bill set to a given register"
  (partial combine-bills +))

(def take-bills
  "Remove a bill set from a given register

   This function allows for a negative number of bills
   in the register"
  (partial combine-bills -))

(defn dollar-value
  "Return the total dollar value for a given bill set"
  [reg]
  (reduce + 0 (map (partial apply *) reg)))

(defn show
  "Return a string representation of a bill set"
  ([reg] (show reg true))
  ([reg show-dollars?]
   (let [total (dollar-value reg)
         state (string/join " " (map reg denoms))]
     (if show-dollars?
       (format "$%s %s" total state)
       state))))

(defn merge-lists
  "Merge sorted lists based on f"
  ([f l r] (merge-lists f l r []))
  ([f [l & lr :as lx] [r & rr :as rx] result]
   (cond
     (nil? l)
     (concat result rx)

     (nil? r)
     (concat result lx)

     (< (f l) (f r))
     (recur f lr rx (conj result l))

     :else
     (recur f lx rr (conj result r)))))

(defn change
  "For a given register and requested amount of change
  {:change <bill set representing the change>
   :register <register less the change>}"
  [reg amt]
  (let [initial-option {:remaining-amount amt
                        :change {}
                        :register reg}]
    (loop [[option & remaining-options :as all-options] #{initial-option}]
      ;; There are no options left for us to consider,
      ;; we can't make change for this amount
      (if (nil? option)
        {:error :no-change
         :register reg}
        ;; all-options is sorted with the smallest
        ;; remaining amounts first so we can just check if we hit a
        ;; zero amount
        (if (zero? (:remaining-amount option))
          option
          (let [{:keys [register remaining-amount change]} option
                ;; If we haven't solved we'll look through all of
                ;; our next options: each remaining bill greater than
                ;; the current remaining amount
                options (keep
                         (fn [[denom bills]]
                           (when (and (>= remaining-amount denom)
                                      (pos? bills))
                             {:remaining-amount (- remaining-amount denom)
                              :change (add-bills change {denom 1})
                              :register (take-bills register {denom 1})}))
                         register)]
            ;; Clojure's sequence functions (like keep) are lazy but in
            ;; this case we're in a strict recur-loop so we're going to
            ;; force the computation here
            ;;
            ;; Since both options and remaining options are sorted
            ;; we can quickly merge them into a sorted list
            (recur (doall (merge-lists :remaining-amount options remaining-options)))))))))

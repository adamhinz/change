# Change Me

A library designed to calculate change for your sales system

## Usage

To build this project you'll need `leiningen`. If you're on a Mac that's
as simple as:

```
brew install leiningen
```

Otherwise you can learn more about installing it at
[http://leiningen.org/#install](http://leiningen.org/#install).

This project can be used at a REPL via:

```
lein repl
```

You can run the example from the spec like:

```clojure
user=> (use 'change.repl)
nil
user=> (repl-example)
$68 1 2 3 4 5
$128 2 4 6 4 10
$43 1 0 3 4 0
0 0 1 3 0
$32 1 0 2 1 0
sorry
{20 1, 10 0, 5 2, 2 1, 1 0}
```

## Tests

There is a full test suite that verifies the correctness of the
library. The test suite also includes a generative test for extra
surety.

To execute the test suite run:

```
lein test
```

## License

Copyright © 2016 Adam Hinz

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
